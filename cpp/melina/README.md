### Meli C++ Lessons

To clone the repository (only needs to be done once):
```sh
# Clone the repository using git
git clone https://github.com/0xdec/meli_cpp.git
```

To update the repository:
```sh
# Navigate into the repository directory using cd (change directory)
cd meli_cpp

# Update the files in the repository
git pull
```

To build and run the hello_world example program:
```sh
# Navigate into the hello_world directory
cd meli_cpp/hello_world

# Compile the program using g++ (the C++ compiler)
g++ main.cpp -o hello_world

# Run hello_world (remember that "." is the current directory)
./hello_world
```
